# Pi-COMMS

This is a Raspberry Pi 3 / 3+ hat for RF communications using AX5043 transceiver and MMZ09332B power amplifier.

## Instructions

* Make sure to produce the PCB on 0.8mm thickness (RF lines calculations were done assuming that)
* For test code using the board you can use [BrandeburgTech DigitalTxRxRpi code](https://github.com/BrandenburgTech/DigitalTxRxRPi)

## License

Licensed under the [CERN OHLv1.2](LICENSE) 2018 Libre Space Foundation.